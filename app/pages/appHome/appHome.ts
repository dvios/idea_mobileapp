import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PostCard } from '../appComponents/postCard/PostCard';

@Component({
  templateUrl: 'build/pages/appHome/appHome.html',
  directives: [PostCard]
})
export class AppHome {
    posts: Array<{image: string, title: string, text: string}>;

    constructor(public navCtrl: NavController, navParams: NavParams) {
        this.posts = [{
            image: 'http://ultraimg.com/images/2016/07/29/Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg',
            title: 'some crazy title',
            text: 'This is and awesome card description prepared by DVios'
        }];
    }
}