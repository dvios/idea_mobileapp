import { IONIC_DIRECTIVES } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Post } from './Post';

@Component({
    selector: 'dvios-post-card',
    templateUrl: 'build/pages/appComponents/PostCard/postCard.html',
    directives: [IONIC_DIRECTIVES]
})

export class PostCard {
    @Input()
    post: Post;
}