export class Post {
    // use a base64 encoded string
    image: string;
    title: string;
    text: string;
}